#include <ros/ros.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointField.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/point_field_conversion.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

template<typename T>
void loadParameter(ros::NodeHandle& _private_nh, const std::string _param_name, T& _var, const T _default_val);
void newPointcloudCallback(const sensor_msgs::PointCloud2::ConstPtr& _pointcloud);
ros::Publisher pointcloudPublisher;

std::string frame_id;
int num_of_rings;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "velodyne_ring_test");

    ros::NodeHandle nh;
    ros::NodeHandle private_nh("~");

    loadParameter(private_nh, "frame_id", frame_id, std::string("velodyne") );
    loadParameter(private_nh, "rings", num_of_rings, 16);

    int queueSize = 5;

    ros::Subscriber pointcloudSubscriber = nh.subscribe<sensor_msgs::PointCloud2>("pointcloud", queueSize, newPointcloudCallback);
    pointcloudPublisher = nh.advertise<PointCloud>("pcl_pointcloud", num_of_rings);

    ros::spin();

    return 0;
}


static int recv_cloud_cnt = 0;
void newPointcloudCallback(const sensor_msgs::PointCloud2::ConstPtr& _pointcloud)
{
    std::vector<PointCloud> clouds_msg(num_of_rings);
    sensor_msgs::PointCloud2ConstIterator<float> itr_x(*_pointcloud, "x");
    sensor_msgs::PointCloud2ConstIterator<float> itr_y(*_pointcloud, "y");
    sensor_msgs::PointCloud2ConstIterator<float> itr_z(*_pointcloud, "z");
    sensor_msgs::PointCloud2ConstIterator<uint16_t> itr_ring(*_pointcloud, "ring");
    for (itr_x; itr_x != itr_x.end(); ++itr_x, ++itr_y, ++itr_z, ++itr_ring)
    {
        clouds_msg[*itr_ring].push_back(pcl::PointXYZ(*itr_x, *itr_y, *itr_z) );
    }

    for (int i = 0; i < num_of_rings; i++)
    {
        pcl_conversions::toPCL(ros::Time::now(), clouds_msg[i].header.stamp);
        clouds_msg[i].header.frame_id = frame_id;
        clouds_msg[i].height = 1;
        clouds_msg[i].is_dense = false;
        pointcloudPublisher.publish(clouds_msg[i]);
    }
}


template<typename T>
void loadParameter(ros::NodeHandle& _nh, const std::string _param_name, T& _var, const T _default_val)
{
  std::string result_name;

  if (_nh.searchParam(_param_name, result_name))
  {
    _nh.getParam(result_name, _var);
  }
  else
  {
    _var = _default_val;
    ROS_ERROR_STREAM("Could not find '" << _param_name << "' parameter. " <<
                     "Setting the param with default value: " << _var);
  }
}